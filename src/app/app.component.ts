import { Component,OnInit,Inject,Renderer2  } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { ServiceService } from './service.service';
import { AuthService } from './_services/auth.service';
import { TokenStorageService } from './_services/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  theme: Theme = 'light-theme';
  logged: boolean = false;
  constructor(private service : ServiceService,private router:Router, @Inject(DOCUMENT) private document: Document,
  private renderer: Renderer2,private _authService:AuthService,private tokenStorageService:TokenStorageService){
    this.service.getData().subscribe(data=>{
      console.warn(data)
      
    })

  }
  ngOnInit() {
    this.initializeTheme();
    this.canActivate()
  }

  switchTheme() {
    this.document.body.classList.replace(
      this.theme,
      this.theme === 'light-theme'
        ? (this.theme = 'dark-theme')
        : (this.theme = 'light-theme')
    );
  }

  initializeTheme = (): void =>
    this.renderer.addClass(this.document.body, this.theme);
   
    canActivate() {
      if(this._authService.loggedIn()){
        this.logged=true
      }
      
      
}
logout(): void {
  this.tokenStorageService.signOut();
 
}

}
export type Theme = 'light-theme' | 'dark-theme';


