import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

export class Group {
  constructor(
  ) {
  }
}
@Component({
  selector: 'app-groupes',
  templateUrl: './groupes.component.html',
  styleUrls: ['./groupes.component.css']
})

export class GroupesComponent implements OnInit {
  groupes : Array<any>=[];
  name: string | any; 
  a : Array<any>=[];
  constructor(private httpClient :HttpClient,private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.Search();
    this.getGroups();
  }
  getGroups(){
    this.httpClient.get<any>('https://gitlab.com/api/v4/groups/?private_token=glpat-nrUF2R_t1o7vsZjrosUL').subscribe(
      response => {
        
        this.groupes = response;
        this.a=response
      }
    );

}
Search(){
            
  if(this.name !=""){
    this.groupes=this.groupes.filter((res)=>{
    return res.name.toLowerCase().match(this.name)
    })
  }
    else if (this.name==""){
    this.groupes=this.a
    }

    }
}
