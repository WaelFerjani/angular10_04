import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';


export class Projet {
  constructor(
    public id: number,
    public name: string,
    public default_branch: string,
     public readme_url: string,
  ) {
  }
}

@Component({
  selector: 'app-mat-card',
  templateUrl: './mat-card.component.html',
  styleUrls: ['./mat-card.component.css']
})
export class MatCardComponent implements OnInit {
  projets : Array<any>=[];
  a : Array<any>=[];
  name: string | any; 
  constructor(private httpClient :HttpClient,private route:ActivatedRoute) {}

  ngOnInit(): void {
    this.getProjets();
  }

 getProjets(){
   const id=String(this.route.snapshot.paramMap.get('id'));
   console.log(id);
    this.httpClient.get<any>('https://gitlab.com/api/v4/groups/'+id+'/projects?private_token=glpat-nrUF2R_t1o7vsZjrosUL').subscribe(
      response => {        
        this.projets = response;
        this.a=response
      }
    );
      
  
    }
    Search(){
            
            if(this.name !=""){
              this.projets=this.projets.filter((res)=>{
              return res.name.toLowerCase().match(this.name)
              })
            }
              else if (this.name==""){
              this.projets=this.a
              }
      
              }
}
            



