import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



import { MatCardModule } from '@angular/material/card';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatCardComponent } from './mat-card/mat-card.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { RouterModule } from '@angular/router';
import { GroupesComponent } from './groupes/groupes.component';
import { ReleasesComponent } from './releases/releases.component';

import { FormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';
import { ProjetsComponent } from './projets/projets.component';
import { ReadmeComponent } from './readme/readme.component';
import { AngularFileUploaderModule } from "angular-file-uploader";
import{MatDialogModule} from '@angular/material/dialog';
import { PopUpComponent } from './pop-up/pop-up.component';
import { ArtifactsComponent } from './artifacts/artifacts.component';
import { RepositoryComponent } from './repository/repository.component';
import { ChildComponent } from './child/child.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component'; 
import { HomeComponent } from './home/home.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import { BoardModeratorComponent } from './board-moderator/board-moderator.component';
import { BoardUserComponent } from './board-user/board-user.component';
import { AuthGuard } from './auth.guard';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MatCardComponent,
    GroupesComponent,
    ReleasesComponent,
    ProjetsComponent,
    ReadmeComponent,
    PopUpComponent,
    ArtifactsComponent,
    RepositoryComponent,
    ChildComponent,
    LoginComponent,
    ProfileComponent,
    RegisterComponent,

    


    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatGridListModule,
    FormsModule,
    MatTabsModule,
    AngularFileUploaderModule,
    MatPaginatorModule,
    MatDialogModule,
    RouterModule.forRoot([
    {path:'',component:GroupesComponent,canActivate:[AuthGuard]},
    {path:'groupe/:id',component: MatCardComponent,canActivate:[AuthGuard]},
    {path:'groupes/:searchTerm',component:GroupesComponent,canActivate:[AuthGuard]},
    {path:'projet/:id',component: ReleasesComponent,canActivate:[AuthGuard]},
    {path: ':id',component: ReadmeComponent,canActivate:[AuthGuard]},
    {path:'projet/:id',component: ReadmeComponent,canActivate:[AuthGuard]},
    {path:'navbar',component:NavbarComponent,canActivate:[AuthGuard]},
    {path:'repository/:key',component:RepositoryComponent,canActivate:[AuthGuard] },
    { path: 'repository/:key/:folder',component:ChildComponent,canActivate:[AuthGuard]},

    
    {path:'*',redirectTo:'', pathMatch: 'full',canActivate:[AuthGuard]},
    

    ])

  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
