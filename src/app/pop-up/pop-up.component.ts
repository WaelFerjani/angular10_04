import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.css']
})
export class PopUpComponent implements OnInit {

  constructor(private httpClient :HttpClient,private route:ActivatedRoute) { }

  ngOnInit(): void { 
    this.getreadme();
  }
  readme : Array<string>=[];
  getreadme(){
    const id=String(this.route.snapshot.paramMap.get('id'));

    this.httpClient.get<any>('https://gitlab.com/api/v4/projects/'+id+'/repository/files/README.md?ref=main').subscribe(
      response => {
        this.readme.push(atob(response.content));
      }

      
    );
    console.log(this.readme)
  }

}
