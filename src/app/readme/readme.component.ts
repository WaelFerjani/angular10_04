import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-readme',
  templateUrl: './readme.component.html',
  styleUrls: ['./readme.component.css']
})
export class ReadmeComponent implements OnInit {


  constructor(private httpClient :HttpClient,private route:ActivatedRoute) { }
  readme : Array<string>=[];

  ngOnInit(): void {
    this.getreadme()
  }


  getreadme(){
   
    const headers = new HttpHeaders(

       "Authorization: Bearer glpat-nrUF2R_t1o7vsZjrosUL"
    );

    const id=String(this.route.snapshot.paramMap.get('id'));
    this.httpClient.get<any>('https://gitlab.com/api/v4/projects/'+id+'/repository/files/README.md?ref=master', { headers: headers }).subscribe(
      response => {
        this.readme.push(atob(response.content));
      }

      
    );
    console.log(this.readme)
  }

}
