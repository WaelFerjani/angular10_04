import { ArtifactsComponent } from './../artifacts/artifacts.component';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import { PopUpComponent } from 'src/app/pop-up/pop-up.component';
import { ReadmeComponent } from '../readme/readme.component';
import { Router } from '@angular/router';
export class Releases {
  constructor(
  
  ) {
  }
}
@Component({
  selector: 'app-releases',
  templateUrl: './releases.component.html',
  styleUrls: ['./releases.component.css']
})
export class ReleasesComponent implements OnInit {
  releases : Array<any>=[];
  readme : Array<any>=[];
  name: string | any; 
  a : Array<any>=[];
  constructor(private httpClient :HttpClient,private route:ActivatedRoute,private dialogRef:MatDialog,private router:Router) { }

  ngOnInit(): void {
    this.getReleases();
    this.Search();
  
    
  }
  getReleases(){
    const id=String(this.route.snapshot.paramMap.get('id'));
    console.log(id);
     this.httpClient.get<any>('https://gitlab.com/api/v4/projects/'+id+'/releases?private_token=glpat-nrUF2R_t1o7vsZjrosUL').subscribe(
       response => {        
         this.releases = response;
         this.a=response
       }
     );
     }
     
     Search(){
            
      if(this.name !=""){
        this.releases=this.releases.filter((res)=>{
        return res.name.toLowerCase().match(this.name)
        })
      }
        else if (this.name==""){
        this.releases=this.a
        }
    
        }

        openDialog(){
          const id=String(this.route.snapshot.paramMap.get('id'));
          this.dialogRef.open(ReadmeComponent,{
            height: '600px',
            width: '850px',
          })
            this.router.navigate(['',id])
        }
        getRepositories(){
         
          this.dialogRef.open(ArtifactsComponent,{
            height: '600px',
            width: '850px',
          })
          this.router.navigate(['route'])
         
        }
}
