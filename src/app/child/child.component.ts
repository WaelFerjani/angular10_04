import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  repository : Array<any>=[];
  ch : Array<string>=[];
  children: Array<any>=[];
  key:any
  folder:any

  constructor(private httpClient :HttpClient,private route:ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
    this.getRepository()
  }
  getRepository(){
    const headers = new HttpHeaders({
      'X-JFrog-Art-Api':'AKCp8krANVz5hZFppDUPhrVT79G6A6oq1Qq7BS368zM6NEd9mMC96A2ATygoE9kckLjgoJQE6'
    }); 
    this.route.paramMap.subscribe((params : ParamMap)=> {  
      this.key=params.get('key');  
      this.folder=params.get('folder')
      console.log(this.key)
      console.log(this.folder)
      this.httpClient.get<any>('/api/storage/'+this.key+'/'+this.folder, { headers: headers }).subscribe(
        response => {        
          this.repository = response
          this.children=response.children 
          console.log(this.repository)
          console.log(this.children)
        }
      );
      
    });
  
    
     }

}
