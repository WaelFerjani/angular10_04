import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';



@Component({
  selector: 'app-repository',
  templateUrl: './repository.component.html',
  styleUrls: ['./repository.component.css']
})
export class RepositoryComponent implements OnInit {
  repository : Array<any>=[];
  ch : Array<string>=[];
  children: Array<any>=[];
  key:any
  url!: string;
  constructor(private httpClient :HttpClient,private route:ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
    this.getRepository()
  }
 
  getRepository(){
    const headers = new HttpHeaders({
      'X-JFrog-Art-Api':'AKCp8krANVz5hZFppDUPhrVT79G6A6oq1Qq7BS368zM6NEd9mMC96A2ATygoE9kckLjgoJQE6'
    }); 
    this.route.paramMap.subscribe((params : ParamMap)=> {  
      this.key=params.get('key');  
      
      this.httpClient.get<any>('/api/storage/'+this.key, { headers: headers }).subscribe(
        response => {        
          this.repository = response
          this.children=response.children
           
          console.log(this.repository)
          console.log(this.children)
        }
      );
      
    });
  
    
     }
     onClick(child:any){
       console.log(child)
       let a= child.value.uri.substr(1,child.value.uri.length)
       console.log(a);
     this.router.navigate([a],{relativeTo:this.route}) ;
     }
}
