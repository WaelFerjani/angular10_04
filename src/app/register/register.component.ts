import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: any = {
    username: null,
    email: null,
    password: null,
    git_token:null,
    artifact_token:null
  };
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  roles: string[] = [];

  constructor(private authService: AuthService,private router:Router,private tokenStorage:TokenStorageService) { }

  ngOnInit(): void {
    if (this.authService.loggedIn()) {
      this.router.navigate(['']);
   }
  }

  onSubmit(): void {
    const { username, email, password,git_token,artifact_token } = this.form;

    this.authService.register(username, email, password,git_token,artifact_token).subscribe(
      data => {
        this.reloadPage()
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.roles = this.tokenStorage.getUser().roles;

      

      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
  reloadPage() {
    window.location.reload()
  }

}
