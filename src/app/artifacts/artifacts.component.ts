import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { RepositoryComponent } from '../repository/repository.component';

@Component({
  selector: 'app-artifacts',
  templateUrl: './artifacts.component.html',
  styleUrls: ['./artifacts.component.css']
})
export class ArtifactsComponent implements OnInit {
  repositories : Array<any>=[];
  component:RepositoryComponent | any
  url:string | undefined
  constructor(private httpClient :HttpClient,private dialogRef:MatDialog) { }

  ngOnInit(): void {
    this.getRepositories();
   
  }
  getRepositories(){

    const headers = new HttpHeaders({
      'X-JFrog-Art-Api':'AKCp8krANVz5hZFppDUPhrVT79G6A6oq1Qq7BS368zM6NEd9mMC96A2ATygoE9kckLjgoJQE6'
    });

    this.httpClient.get<any>('/api/repositories', { headers: headers }).subscribe(
      response => {
        this.repositories= response;
        this.url=response.url
        console.log(this.url)
        console.log(this.repositories);
      }
    );
}
getRepository(){
         
  this.dialogRef.open(RepositoryComponent,{
    height: '600px',
    width: '850px',
  })
  
 
}
}